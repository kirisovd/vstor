MANY_TARGETS = '''<?xml version="1.0"?>
<targets>
<target>
<iqn>i.q:n</iqn>
<portals>
<portal>1</portal>
<portal>2</portal>
</portals>
<host>6e924ca43bdf4e10</host><status>running</status>
<lun>
<id>1</id>
<size>1024M</size>
<used>1M</used>
<online>Yes</online>
<attr></attr>
</lun>
<lun>
<id>2</id>
<size>1024M</size>
<used>1M</used>
<online>Yes</online>
<attr></attr>
</lun>
<initiators>
</initiators>
</target>
<target>
<iqn>iqn.2014.06.com.vstorage:test1</iqn>
<portals>
<portal>172.29.225.171</portal>
</portals>
<host>d3ac2ef1d0604693</host><status>remote</status>
<lun>
<id>3</id>
<size>unknown size</size>
<used>1M</used>
<online>No</online>
<attr></attr>
</lun>
</target>
<target>
<iqn>iqn.2014.06.com.vstorage:test2</iqn>
<portals>
<portal>172.29.225.172</portal>
</portals>
<host>d3ac2ef1d0604693</host><status>stopped</status>
</target>
</targets>'''

ONE_TARGET = '''<?xml version="1.0"?>
<targets>
<target>
<iqn>i.q:n</iqn>
<portals>
<portal>1.1.1.1</portal>
</portals>
<host>hhh</host><status>running</status>
<lun>
<id>1</id>
<size>1024M</size>
<used>1M</used>
<online>Yes</online>
<attr></attr>
</lun>
<initiators>
</initiators>
</target>
</targets>
'''

TARGET_NO_LUNS = '''<?xml version="1.0"?>
<targets>
<target>
<iqn>i.q:n</iqn>
<portals>
<portal>1.1.1.1</portal>
</portals>
<host>hhh</host><status>running</status>
<initiators>
</initiators>
</target>
</targets>
'''

TARGET_TWO_LUNS = '''<?xml version="1.0"?>
<targets>
<target>
<iqn>i.q:n</iqn>
<portals>
<portal>1.1.1.1</portal>
</portals>
<host>hhh</host><status>running</status>
<lun>
<id>1</id>
<size>100M</size>
<used>10M</used>
<online>Yes</online>
<attr></attr>
</lun>
<lun>
<id>2</id>
<size>102G</size>
<used>20M</used>
<online>No</online>
<attr></attr>
</lun>
<initiators>
</initiators>
</target>
</targets>
'''

SNAPSHOT_LIST = '''CREATED             C UUID                                 PARENT_UUID                          DESCRIPTION
2017-03-02 14:03:54   afb2683a-c239-4f77-acd5-4514383f6aa8 00000000-0000-0000-0000-000000000000 None
2017-03-02 16:59:31 * ab957d22-124b-403b-9d4b-94657514fc39 afb2683a-c239-4f77-acd5-4514383f6aa8 Desc'''

EMPTY_SNAPSHOT_LIST = '''CREATED             C UUID                                 PARENT_UUID                          DESCRIPTION'''

TARGET_DICT_TEST = '''<?xml version="1.0"?>
<targets>
<target>
<iqn>IQN</iqn>
<portals>
<portal>ip</portal>
</portals>
<host>HOST</host><status>running</status>
<lun>
<id>13</id>
<size>23G</size>
<used>1M</used>
<online>Yes</online>
<attr></attr>
</lun>
<initiators>
</initiators>
</target>
</targets>
'''

SNAPSHOT_LIST_DICT_TEST = '''CREATED             C UUID                                 PARENT_UUID                          DESCRIPTION
2017-03-02 14:03:54   abc def None
2017-03-02 16:59:31 * xyz abc Desc'''
