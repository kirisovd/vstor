import re
import os
import sys
import subprocess
from datetime import datetime
from collections import namedtuple

import logbook
from lxml import etree

ShellCmdResult = namedtuple('CommandResult', 'exit_code stdout stderr')
logbook.StreamHandler(sys.stdout).push_application()

VSTOR_CMD = 'vstorage-iscsi'


class VStorCommandFailed(Exception):
    pass


class BadXML(Exception):
    pass


class TargetStatus(object):
    RUNNING = 'running'
    STOPPED = 'stopped'
    UNREGISTERED = 'unregistered'
    REMOTE = 'remote'

    @classmethod
    def fromstring(cls, state):
        states = [getattr(cls, attr) for attr in dir(cls) if not attr.startswith('__')]
        if state in states:
            return state
        else:
            raise ValueError('Invalid status for iSCSI target: {}'.format(state))


def run_shell_command(cmd):
    """Executes given command in linux shell.

    Returns tuple with exit code, stdout and stderr.
    """

    popen_kwargs = {
        'stdin': subprocess.PIPE,
        'stdout': subprocess.PIPE,
        'stderr': subprocess.PIPE,
        'shell': True,
        'universal_newlines': True,
        'env': os.environ.copy()
    }

    logbook.debug('Executing: {}', cmd)
    process = subprocess.Popen(cmd, **popen_kwargs)
    process.wait()
    return ShellCmdResult(
        process.returncode,
        process.stdout.read(),
        process.stderr.read()
    )


def run_vstor_command(command, *args, **kwargs):
    """Executes vstorage-iscsi command with given arguments."""

    def make_arg(key, value=None):
        args = []

        args.append('--{}'.format(key))
        if value is not None:
            args.append(value)

        return args

    params = [VSTOR_CMD, command]

    for key in sorted(list(kwargs)):
        formatted_key = key.replace('_', '-')
        value = kwargs[key]

        if isinstance(value, list):
            for param in value:
                params.extend(make_arg(formatted_key, param))
        else:
            params.extend(make_arg(formatted_key, value))

    for key in args:
        if key:
            params.extend(make_arg(key))

    result = run_shell_command(params)
    exc = parse_output_for_errors(result)
    if exc:
        logbook.error('Command "{}" failed: {}', params, result)
        raise exc

    return result


def parse_output_for_errors(cmd_result):
    """Parses stderr of command's result if exit code is not 0.
    If some pattern matched, raises a VStorCommandFailed.
    """

    if cmd_result.exit_code == 0:
        return

    errors_mapping = {
        1: {
            r'Address \'.+?\' is wrong': None,
            r'Unable build iqn for name \'.+?\'': None,
            r'Unknown option \'.+?\'': None
        },
        5: {
            r'Can\'t find CHAP account \'.+?\'': None
        }
    }

    errors_for_code = errors_mapping.get(cmd_result.exit_code)
    if errors_for_code:
        for pattern, description in errors_for_code.iteritems():
            match = re.search(pattern, cmd_result.stderr)
            if match:
                exc_message = description if description else match.group()
                return VStorCommandFailed(exc_message)

    return VStorCommandFailed('VStor command failed with code: {}'.format(cmd_result.exit_code))


class VirtuozzoStorage(object):

    def list_targets(self):
        """Lists all iSCSI targets.

        Returns a list of IscsiTarget objects."""

        res = run_vstor_command('list', 'all', 'xml')
        targets = []
        for target_dict in self.get_targets_dict():
            target_dict.pop('luns')
            targets.append(IscsiTarget(**target_dict))

        return targets

    def create_target(self, name, portals, user=None):
        """Creates iSCSI target.

        Returns IscsiTarget object.
        """

        if not user:
            res = run_vstor_command('create', name=name, address=portals)
        else:
            res = run_vstor_command('create', name=name, address=portals, user=user)

        iqn = res.stdout.strip().replace('IQN: ', '')
        raw_xml = run_vstor_command('list', 'xml', target=iqn).stdout
        target_dict = get_targets_data_from_xml(raw_xml)[0]
        target_dict.pop('luns')
        return IscsiTarget(**target_dict)

    def get_targets_dict(self):
        """Return dict with detailed information about all
        iSCSI targets found."""

        res = run_vstor_command('list', 'all', 'xml')
        return get_targets_data_from_xml(res.stdout)


class VStorExecMixin(object):
    """Avoid duplication of code while calling vstorage-iscsi command
    in some objects."""

    def _run_cmd(self, *args, **kwargs):

        positionals = list(args)

        if kwargs.get('force_mode') is not None:
            force = kwargs.pop('force_mode')
            if force:
                positionals.append('force')

        return run_vstor_command(*positionals, **kwargs)


class LunSnapshot(VStorExecMixin):
    """A LUN's snapshot wrapper."""

    def __init__(self, lun, uuid=None, parent_uuid=None, description='', is_current=False, creation_date=None):
        self.lun = lun
        self.uuid = uuid
        self.parent_uuid = parent_uuid
        self.description = description
        self.is_current = is_current
        self.creation_date = creation_date

    def __repr__(self):
        return '<Snapshot uuid={} target={} lun={}>'.format(self.uuid, self.lun.target.iqn, self.lun.lun_id)

    def _run_cmd(self, *args, **kwargs):
        kwargs['uuid'] = self.uuid
        return super(LunSnapshot, self)._run_cmd(*args, **kwargs)

    def delete(self):
        """Removes snapshot."""

        self._run_cmd('snapshot-delete')

    def restore(self):
        """Restores snapshot making it current inside the LUN.
        Requires restart of iSCSI target.
        """

        self.lun.target.stop()
        self._run_cmd('snapshot-switch')
        self.lun.target.start()

    def to_dict(self):
        """Returns a dictionary with information about snapshot."""

        return {
            'uuid': self.uuid,
            'parent_uuid': self.parent_uuid,
            'description': self.description,
            'current': self.is_current,
            'creation_date': str(self.creation_date)
        }


class Lun(VStorExecMixin):
    """A LUN wrapper."""

    def __init__(self, target, lun_id, is_online=False, size=0, used=0):
        self.target = target
        self.lun_id = int(lun_id)
        self.size = int(size)
        self.used = int(used)
        self.is_online = is_online

    def _run_cmd(self, *args, **kwargs):
        kwargs['target'] = self.target.iqn
        kwargs['lun'] = self.lun_id
        return super(Lun, self)._run_cmd(*args, **kwargs)

    def __repr__(self):
        return '<Lun id={} in {}>'.format(self.lun_id, self.target.iqn)

    def delete(self):
        """Removes LUN."""

        self._run_cmd('lun-del')

    def create_snapshot(self, uuid=None, description=None):
        """Takes snapshot from LUN."""

        optionals = {}
        if uuid:
            optionals['uuid'] = uuid
        if description:
            optionals['description'] = description

        self._run_cmd('snapshot-create', **optionals)

    def list_snapshots(self):
        """Lists all snapshots related to LUN.
        Returns a list with LunSnapshot objects.
        """

        pattern = (
            r'(?P<date>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}) '
            r'(?P<used>\*)?  ?(?P<uuid>.+?) (?P<parent>.+?) (?P<description>.+?$)'
        )

        output = self._run_cmd('snapshot-list', 'show-desc').stdout

        snapshots = []
        for line in output.strip().split('\n')[1:]:
            match = re.match(pattern, line)

            description = match.group('description')
            if description == 'None':
                description = ''

            is_current = match.group('used') is not None
            date = datetime.strptime(match.group('date'), '%Y-%m-%d %H:%M:%S')

            snapshots.append(
                LunSnapshot(
                    lun=self,
                    uuid=match.group('uuid'),
                    parent_uuid=match.group('parent'),
                    description=description,
                    is_current=is_current,
                    creation_date=date
                )
            )

        return snapshots

    def increase_size(self, new_size):
        """Increases size of LUN."""

        if new_size < self.size:
            raise ValueError('New size should be bigger than {} MB'.format(self.size))

        self._run_cmd('lun-grow', size='{}M'.format(new_size))
        self.size = new_size

    def to_dict(self):
        """Returns a dictionary with information about LUN
        and its snapshots."""

        return {
            'id': self.lun_id,
            'size': self.size,
            'used': self.used,
            'online': self.is_online,
            'snapshots': [snapshot.to_dict() for snapshot in self.list_snapshots()]
        }


class IscsiTarget(VStorExecMixin):
    """iSCSI Target wrapper."""

    def __init__(self, iqn, status=None, portals=None, host=None):
        self._data = {}
        self._data.update({k: v for k, v in locals().iteritems() if k != 'self'})

    def _run_cmd(self, *args, **kwargs):
        kwargs['target'] = self.iqn
        return super(IscsiTarget, self)._run_cmd(*args, **kwargs)

    def _refresh_data(self):
        res = self._run_cmd('list', 'xml')
        try:
            target_info = get_targets_data_from_xml(res.stdout)[0]
        except IndexError:
            raise BadXML('Could not parse XML returned by vstorage-iscsi list')

        self._data.update(target_info)

    def _get_data(self, key):
        if self._data.get(key) is None:
            self._refresh_data()

        return self._data[key]

    @property
    def iqn(self):
        return self._data['iqn']

    @property
    def portals(self):
        return self._get_data('portals')

    @property
    def status(self):
        return self._get_data('status')

    @property
    def host(self):
        return self._get_data('host')

    def __repr__(self):
        return '<iSCSI Target {}>'.format(self.iqn)

    def start(self):
        """Starts iSCSI target."""

        self._run_cmd('start')
        self._data['status'] = TargetStatus.RUNNING

    def stop(self, force=False):
        """Stops iSCSI target."""

        self._run_cmd('stop', force_mode=force)
        self._data['status'] = TargetStatus.STOPPED

    def delete(self, force=False):
        """Removes  iSCSI target."""

        self.stop()
        self._run_cmd('delete', force_mode=force)

    def unregister(self, force=False):
        """Unregisters iSCSI target from host."""

        self.stop(force=force)
        self._run_cmd('unregister')
        self._data['status'] = TargetStatus.UNREGISTERED

    def register(self, force=False):
        """Register iSCSI target on current host."""

        self._run_cmd('register', force_mode=force)
        self._data['status'] = TargetStatus.STOPPED

    def create_lun(self, lun_id, size, scsi_sn=None, vstor_attrs=None):
        """Creates LUN belonging to iSCSI target."""

        optionals = {}
        if scsi_sn:
            optionals['serial'] = scsi_sn
        if vstor_attrs:
            optionals['vstorage_attr'] = vstor_attrs

        try:
            size_in_mb = '{}M'.format(int(size))
        except ValueError:
            raise ValueError('{} should be integer'.format(size))

        self._run_cmd(
            'lun-add',
            lun=lun_id,
            size=size_in_mb,
            **optionals
        )

        for lun in self.list_luns():
            if lun.lun_id == lun_id:
                return lun

    def list_luns(self):
        """Lists all LUNs belonging to iSCSI target.
        Returns a list with Lun objects.
        """

        res = self._run_cmd('list', 'xml')
        target_dict = get_targets_data_from_xml(res.stdout)[0]

        luns = []
        for lun_dict in target_dict.pop('luns'):
            lun_dict['target'] = self
            luns.append(Lun(**lun_dict))

        return luns

    def to_dict(self):
        """Returns a dictionary with information about iSCSI target,
        its LUNs and snapshots of each LUN."""

        return {
            'host': self.host,
            'iqn': self.iqn,
            'status': self.status,
            'portals': self.portals,
            'luns': [lun.to_dict() for lun in self.list_luns()]
        }


def parse_size(size_value):
    """Converts LUN's size from vstorage-iscsi's output to int value."""

    if size_value == 'unknown size':
        return -1
    elif not re.match(r'\d+?[MG]', size_value):
        raise ValueError('Incorrect value: {}'.format(size_value))

    size_sign = size_value[-1]
    size = int(size_value[:-1])
    return size * {'G': 1024, 'M': 1}[size_sign]


def get_targets_data_from_xml(xml_content):
    """Creates a dict with iSCSI targets information from XML."""

    def find_by_xpath(element, expr, only_first=True, text=True):
        if text:
            expr += '/text()'
        result = element.xpath(expr)
        if not only_first:
            return result
        else:
            return result[0] if len(result) else None

    tree = etree.XML(xml_content)
    targets = []

    for element in tree:

        target_data = {
            'iqn': find_by_xpath(element, './iqn'),
            'status': TargetStatus.fromstring(find_by_xpath(element, './status')),
            'host': find_by_xpath(element, './host'),
            'portals': find_by_xpath(element, './portals/portal', only_first=False),
        }

        luns = []
        for lun_xml in element.xpath('./lun'):
            lun_data = {
                'lun_id': find_by_xpath(lun_xml, './id'),
                'size': parse_size(find_by_xpath(lun_xml, './size')),
                'used': parse_size(find_by_xpath(lun_xml, './used')),
                'is_online': find_by_xpath(lun_xml, './online') == 'Yes'
            }
            luns.append(lun_data)

        target_data['luns'] = luns

        targets.append(target_data)

    return targets
