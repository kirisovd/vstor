import mock
import pytest

import vstor
import xml_test_data
from vstor import IscsiTarget, Lun, LunSnapshot, TargetStatus
from vstor import VirtuozzoStorage as VStor


@pytest.fixture
def mock_run_shell_cmd(monkeypatch, scope='module'):
    mocked_run_cmd = mock.MagicMock()
    mocked_run_cmd.return_value = vstor.ShellCmdResult(0, '', '')
    monkeypatch.setattr('vstor.run_shell_command', mocked_run_cmd)
    return mocked_run_cmd


class MockedShellCmdExecution(object):

    def __init__(self, mock_run_shell_cmd):
        self._mock_run_shell_cmd = mock_run_shell_cmd

    def _create_call(self, cmd):
        # create args list from string
        args = ['vstorage-iscsi']
        for arg in cmd.split():
            try:
                args.append(int(arg))
            except ValueError:
                args.append(arg)

        return mock.call(args)

    def check_calls(self, *expected_commands):
        self.set_return_value()

        calls = [self._create_call(cmd) for cmd in expected_commands]
        actual_calls = self._mock_run_shell_cmd.mock_calls

        assert calls == actual_calls

        self._mock_run_shell_cmd.reset_mock()

    def check_has_call(self, cmd):
        call = self._create_call(cmd)
        self._mock_run_shell_cmd.assert_has_calls([call], any_order=True)

    def set_return_value(self, exit_code=0, stdout='', stderr=''):
        self._mock_run_shell_cmd.return_value = vstor.ShellCmdResult(exit_code, stdout, stderr)

    def set_stdout_output(self, *output_list):
        output = [vstor.ShellCmdResult(0, stdout, '') for stdout in output_list]
        self._mock_run_shell_cmd.side_effect = output


@pytest.fixture
def bash(mock_run_shell_cmd):
    return MockedShellCmdExecution(mock_run_shell_cmd)


def test_popen_calls(monkeypatch):
    mock_popen = mock.MagicMock()
    monkeypatch.setattr('vstor.subprocess.Popen', mock_popen)

    vstor.run_shell_command('ls')
    assert mock_popen.call_args[0] == ('ls',)
    assert mock_popen.call_args[1]['shell']

    vstor.run_shell_command('ls -la /tmp')
    assert mock_popen.call_args[0] == ('ls -la /tmp',)


def test_cmd_composition(bash):
    vstor.run_vstor_command('help')
    bash.check_calls('help')

    vstor.run_vstor_command('start', 'force')
    bash.check_calls('start --force')

    vstor.run_vstor_command('stop', 'a', 'b')
    bash.check_calls('stop --a --b')

    vstor.run_vstor_command('lun-add', key='value')
    bash.check_calls('lun-add --key value')

    vstor.run_vstor_command('cmd', number=42)
    bash.check_calls('cmd --number 42')

    vstor.run_vstor_command('cmd', str="'string'")
    bash.check_calls("cmd --str 'string'")

    vstor.run_vstor_command('cmd', under_score='ok')
    bash.check_calls('cmd --under-score ok')

    vstor.run_vstor_command('cmd', 'a', b='c')
    bash.check_calls('cmd --b c --a')

    vstor.run_vstor_command('cmd', b=['c', 'd'])
    bash.check_calls('cmd --b c --b d')


def test_create_target(bash):
    bash.set_return_value(stdout=xml_test_data.ONE_TARGET)

    target = VStor().create_target('test', ['1.1.1', '2.2.2'])
    bash.check_has_call('create --address 1.1.1 --address 2.2.2 --name test')

    VStor().create_target(name='N', portals=['0.0.0.0'], user='U')
    bash.check_has_call('create --address 0.0.0.0 --name N --user U')


def test_target_properties(bash):
    storage = vstor.VirtuozzoStorage()
    bash.set_stdout_output('IQN: i.q:n', xml_test_data.ONE_TARGET)
    target = storage.create_target(name='test', portals=['1.1.1.1'])
    bash.check_has_call('list --target i.q:n --xml')
    assert target.iqn == 'i.q:n'
    assert target.host == 'hhh'
    assert target.portals == ['1.1.1.1']


def test_start_target(bash):
    target = IscsiTarget('IQN')
    target.start()
    bash.check_calls('start --target IQN')
    assert target.status == TargetStatus.RUNNING


def test_delete_target(bash):
    target = IscsiTarget('IQN')
    target.delete()
    bash.check_calls('stop --target IQN', 'delete --target IQN')
    target.delete(force=True)
    bash.check_calls('stop --target IQN', 'delete --target IQN --force')


def test_stop_target(bash):
    target = IscsiTarget('IQN')
    target.stop()
    bash.check_calls('stop --target IQN')
    assert target.status == TargetStatus.STOPPED

    target.stop(force=True)
    bash.check_calls('stop --target IQN --force')
    assert target.status == TargetStatus.STOPPED


def test_register_target(bash):
    target = IscsiTarget('id')
    target.register()
    bash.check_calls('register --target id')


def test_unregister_target(bash):
    target = IscsiTarget('tg')
    target.unregister()
    bash.check_calls('stop --target tg', 'unregister --target tg')


def test_list_targets(bash):
    bash.set_return_value(stdout=xml_test_data.ONE_TARGET)
    targets = VStor().list_targets()
    assert len(targets) == 1

    target = targets[0]
    assert target.iqn == 'i.q:n'
    assert target.portals == ['1.1.1.1']
    assert target.status == TargetStatus.RUNNING
    assert target.host == 'hhh'

    bash.set_return_value(stdout=xml_test_data.MANY_TARGETS)
    targets = VStor().list_targets()
    assert len(targets) == 3
    assert targets[0].portals == ['1', '2']

    assert targets[0].status == TargetStatus.RUNNING
    assert targets[1].status == TargetStatus.REMOTE
    assert targets[2].status == TargetStatus.STOPPED


def test_list_luns(bash):
    bash.set_return_value(stdout=xml_test_data.TARGET_NO_LUNS)
    targets = VStor().list_targets()

    assert targets[0].list_luns() == []

    bash.set_return_value(stdout=xml_test_data.TARGET_TWO_LUNS)
    target = VStor().list_targets()[0]
    luns = target.list_luns()
    assert len(luns) == 2

    assert luns[0].is_online
    assert not luns[1].is_online

    assert luns[0].size == 100
    assert luns[1].size == 102 * 1024

    lun = luns[0]
    assert lun.lun_id == 1
    assert lun.size == 100
    assert lun.used == 10
    assert lun.is_online == True


def test_create_lun(bash):
    bash.set_return_value(stdout=xml_test_data.TARGET_NO_LUNS)
    target = IscsiTarget('i.q:n')
    lun = target.create_lun(2, 10)
    bash.check_has_call('lun-add --lun 2 --size 10M --target i.q:n')
    target.create_lun(10, 10)
    bash.check_has_call('lun-add --lun 10 --size 10M --target i.q:n')
    target.create_lun(1, '1', scsi_sn='sn', vstor_attrs='a')
    bash.check_has_call('lun-add --lun 1 --serial sn --size 1M --target i.q:n --vstorage-attr a')


def test_delete_lun(bash):
    lun = Lun(lun_id=1, target=IscsiTarget('iqn'))
    lun.delete()
    bash.check_calls('lun-del --lun 1 --target iqn')


def test_lun_growing(bash):
    lun = Lun(lun_id=13, target=IscsiTarget('iqn'), size=1)
    lun.increase_size(2)
    bash.check_calls('lun-grow --lun 13 --size 2M --target iqn')
    assert lun.size == 2
    with pytest.raises(ValueError):
        lun.increase_size(1)
    bash.check_calls()
    assert lun.size == 2


def test_list_snapshots(bash):
    lun = Lun(lun_id=12, target=IscsiTarget('iqn'))
    snapshots = lun.list_snapshots()
    bash.check_calls('snapshot-list --lun 12 --target iqn --show-desc')

    bash.set_return_value(stdout=xml_test_data.SNAPSHOT_LIST)
    snapshots = Lun(lun_id=1, target=IscsiTarget('q')).list_snapshots()
    assert len(snapshots) == 2

    assert not snapshots[0].is_current
    assert snapshots[1].is_current

    assert snapshots[0].description == ''
    assert snapshots[1].description == 'Desc'

    assert snapshots[0].uuid == 'afb2683a-c239-4f77-acd5-4514383f6aa8'
    assert snapshots[1].parent_uuid == snapshots[0].uuid

    bash.set_return_value(stdout=xml_test_data.EMPTY_SNAPSHOT_LIST)
    snapshots = Lun(lun_id=1, target=IscsiTarget('q')).list_snapshots()
    assert snapshots == []


def test_snapshot_delete(bash):
    lun = Lun(lun_id=123, target=IscsiTarget('iqn'))
    LunSnapshot(lun=lun, uuid='abcde').delete()
    bash.check_calls('snapshot-delete --uuid abcde')


def test_snapshot_restore(bash):
    lun = Lun(lun_id=123, target=IscsiTarget('iqn'))
    snapshot = LunSnapshot(lun=lun, uuid='abcde')
    snapshot.restore()
    bash.check_calls(
        'stop --target iqn',
        'snapshot-switch --uuid abcde',
        'start --target iqn'
    )


def test_target_dict_creation(bash):
    bash.set_stdout_output(
        xml_test_data.TARGET_DICT_TEST,
        xml_test_data.TARGET_DICT_TEST,
        xml_test_data.SNAPSHOT_LIST_DICT_TEST
    )
    target = IscsiTarget('IQN')
    expected_dict = {
        'host': 'HOST',
        'iqn': 'IQN',
        'status': TargetStatus.RUNNING,
        'portals': ['ip'],
        'luns': [{
            'id': 13,
            'online': True,
            'size': 23552,
            'used': 1,
            'snapshots': [
                {
                    'current': False,
                    'description': '',
                    'parent_uuid': 'def',
                    'uuid': 'abc',
                    'creation_date': '2017-03-02 14:03:54'
                },
                {
                    'current': True,
                    'description': 'Desc',
                    'parent_uuid': 'abc',
                    'uuid': 'xyz',
                    'creation_date': '2017-03-02 16:59:31'
                }
            ]
        }],
    }
    assert target.to_dict() == expected_dict


def test_size_covertion_from_string():
    assert vstor.parse_size('1M') == 1
    assert vstor.parse_size('1G') == 1024
    assert vstor.parse_size('unknown size') == -1
    with pytest.raises(ValueError):
        vstor.parse_size('incorrect')


def test_exception_values(bash):
    def check_exc_value(exit_code, stderr, expected_value):
        bash.set_return_value(exit_code, stderr=stderr)
        with pytest.raises(vstor.VStorCommandFailed) as exc_info:
            vstor.run_vstor_command('stub')

        assert str(exc_info.value) == expected_value

    stderr = "Address '1' is wrong"
    check_exc_value(1, stderr, stderr)

    stderr = "Unable build iqn for name '@'"
    check_exc_value(1, stderr, stderr)

    stderr = "Unknown option '#'"
    check_exc_value(1, stderr, stderr)

    stderr = "Can't find CHAP account 'q'"
    check_exc_value(5, stderr, stderr)

    stderr = "Sh~ happens"
    check_exc_value(123, stderr, "VStor command failed with code: 123")
